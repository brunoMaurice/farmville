var Cartography = require('./../model/cartography.js').Cartography;

var naturalEvent = {
  weather : {name: 'sun'},
  radius : 6,
  run : function(sockets) {
    setInterval(naturalEvent.rain, 1000*60, sockets);
    setInterval(naturalEvent.disaster, 1000*120, sockets);
  },
  haveEvent : function() {
    var random_percents = Math.floor((Math.random()*100)+1);
  
    return (random_percents > 75) ? true : false;
  },
  rain : function(sockets) {
    if (naturalEvent.haveEvent) {
      console.log('Rain');
      // Besoin d'un algo pour définir la zone où l'event agit

      sockets.emit('naturalevent', {'name' : 'Pluie'});
    }
  },
  disaster : function(sockets) {
    if (naturalEvent.haveEvent) {
      console.log('Disaster');

      sockets.emit('naturalevent', {'name' : 'Pluie de Météorites'});
    }
  }
}

module.exports = naturalEvent;