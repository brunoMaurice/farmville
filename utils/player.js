var Usergame = require('.././model/usergame.js').Usergame;

exports.sendPlayerPos = function(id, callback) {
  Usergame.findOne({_id: id}).exec(function(error, usergame) {
    callback({posX: usergame.posX, posY: usergame.posY});
  });
}

exports.setPos = function(id, x, y) {
  Usergame.findOne({_id: id}).exec(function(error, usergame) {
    usergame.posX = x;
    usergame.posY = y;

    usergame.save();
  });
}