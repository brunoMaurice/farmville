var largeur = 31; // hauteur
var hauteur = 31; // largeur

var Cartography = require('./../model/cartography.js').Cartography;
var Base = require('./../model/base.js').Base;
var Crop = require('./../model/crop.js').Crop;
var User = require('./../model/user.js').User;

module.exports.getMap = function(posX, posY, callback) {
  var matrice = new Array();
  var update = new Array();

  var query = Cartography.where('posX').gte(Math.ceil(posX-largeur/2)-1).lte(Math.ceil(posX+largeur/2)+1)
                         .where('posY').gte(Math.ceil(posY-hauteur/2)-1).lte(Math.ceil(posY+hauteur/2)+1);

  query.populate('_cropid').exec(function(error, cartographys) {
    if (error) {
      console.log(error);
    } else {
      for (i = 0; i < hauteur; i++) {
        for (j = 0; j < largeur; j++) {

          var x = Math.ceil(posX-largeur/2+i);
          var y = Math.ceil(posY-hauteur/2+j);

          var data = getData(cartographys, x, y);

          if (data == null) {
            var humidity = 0,
              fertility = 0;

            if (j == 0) {
              var firstData = getData(cartographys, x, y-1);

              humidity = (firstData == null) ? getRandomInt(0, 100) : getRandomInt(firstData.humidity-10, firstData.humidity+10);
              fertility = (firstData == null) ? getRandomInt(0, 100) : getRandomInt(firstData.fertility-10, firstData.fertility+10);
            } else {
              var lastData = getData(matrice, x, y-1);

              humidity = getRandomInt(lastData.humidity-10, lastData.humidity+10);
              fertility = getRandomInt(lastData.fertility-10, lastData.fertility+10);
            }

            var cartography = data = new Cartography({
              humidity    : humidity,
              fertility   : fertility,
              posX        : x,
              posY        : y
            });

            cartography.save();

            update.push(data);
          }

          matrice.push({
            'crop'      : data._cropid,
            'humidity'  : data.humidity,
            'fertility' : data.fertility,
            'player'    : data._userid,
            'posX'      : data.posX,
            'posY'      : data.posY
          });
        }
      }

      if (callback)
        callback(matrice);

      genBase(update);
    }
  });
}

function getData(object, x, y) {
  var length = object.length;

  for (var i = 0; i < length; i++) {
    if (object[i].posX == x && object[i].posY == y)
      return object[i];
  }

  return null;
}

function genBase(newTiles) {
  if (newTiles.length == 0)
    return false;

  var x = newTiles[0].posX;
  var y = newTiles[0].posY;

  newTiles.shift()

  var query = Base.where('centerX').gte(x-4).lte(x+4)
                  .where('centerY').gte(y-4).lte(y+4);

  query.exec(function(error, bases) {
    if (error) {
      console.log(error);
    } else {
      var baseId = searchBase(bases, x, y);

      if (baseId) {
        updateCartographe(baseId._id, x, y);
        genBase(newTiles);
      } else {
        if (bases.length == 0) {
          var numX = (x < 0) ? -x : x;
          var numY = (y < 0) ? -y : y;

          var base = new Base({
            num     : numX+numY,
            centerX : x,
            centerY : y
          });

          base.increment();

          base.save(function(error, data) {
            if (error) {
              console.log(error);
            } else {
              var query = Cartography.where('posX').gte(x-2).lte(x+2)
                                     .where('posY').gte(y-2).lte(y+2);

              query.exec(function(error, cartographys) {
                if (error) {
                  console.log(error);
                } else {
                  cartographys.forEach(function(carto) {
                      updateCartographe(data._id, carto.posX, carto.posY);
                  });

                  genBase(newTiles);
                }
              });
            }
          });
        } else {
          genBase(newTiles);
        }
      }
    }
  });
}

function searchBase(base, x, y) {
  var length = base.length;

  for (var i = 0; i < length; i++) {
    if (base[i].posX == x && base[i].posY == y)
      return base[i];
  }

  return null;
}

function updateCartographe(baseId, tileX, tileY) {
  Cartography.update({ posX : tileX, posY : tileY }, { _baseid : baseId });
}

function getRandomInt (min, max) {
    var rand = Math.floor(Math.random() * (max - min + 1)) + min;

    if (rand > 100) {
      return 100;
    } else if (rand < 0) {
      return 0;
    } else {
      return rand;
    }
}