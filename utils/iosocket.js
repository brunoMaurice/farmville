var socketio = require('socket.io');
var map = require('./map.js');
var player = require('./player.js');

module.exports = function(server, store, sessionKey, cookieParser) {
  var io = socketio.listen(server, { log: false });

  io.set('authorization', function(data, accept) {
    cookieParser(data, {}, function(err) {
      if (err) {
        accept(err, false);
      } else {
        store.get(data.signedCookies[sessionKey], function(err, session) {
          if (err || !session) {
            accept('Session error', false);
          } else {
            data.session = session;
            accept(null, true);
          }
        });
      }
    });
  });

  io.sockets.on('connection', function(socket) {
    var sess = socket.handshake.session;
    console.log(sess);

    socket.on('getMap', function(message) {
      player.sendPlayerPos(sess.user._usergame, function(pos) {
          console.log(pos);
          map.getMap(pos.posX, pos.posY, function(matrice) {

          var length = matrice.length;

          for (i=0; i<length; i++) {
            socket.emit('setTile', matrice[i]);
          }
        });
      });
    });

    socket.on('getPos', function(message) {
      player.sendPlayerPos(sess.user._usergame, function(pos) {
        socket.emit('setPos', pos);
      });
    })

    socket.on('setPos', function(message) {
      player.setPos(sess.user._usergame, message.x, message.y);
    });
  });

  return io;
}