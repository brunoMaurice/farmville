/**
 * Module dependencies.
 */

var express = require('express'),
  http = require('http'),
  path = require('path'),
  connect = require('express/node_modules/connect');

var sessionKey = 'express.sid',
  secret = 'uslessString',
  store = new express.session.MemoryStore(),
  cookieParser = express.cookieParser(secret);

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser(secret));
app.use(express.session({ secret: secret, key: sessionKey, store: store, httpOnly: false, cookie: { httpOnly: false, maxAge: 600000 }}));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

require('./config/routes.js')(app);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.info('Farm Ville listening on port ' + app.get('port'));
});

require('./config/init.js')();

var io = require('./utils/iosocket.js')(server, store, sessionKey, cookieParser);

var naturalEvent = require('./utils/natural_event.js');
naturalEvent.run(io.sockets);