define(['js/entities/map.js', 'js/entities/player.js', 'js/utils/gamedata.js', 'js/utils/nextAnimationFrame.js'], function(Map, Player, Gamedata) {
  var carte = document.getElementById("gamearea");

  var map = new Map();
  var player = new Player();

  var self = this,
    lastTime = 0;

  this.run = function(socket) {
    $('.dialogfont').remove();
    $('.dialogpoparea').remove();

    var snd = new Audio('mp3/bgsong.mp3'); // buffers automatically when created
    snd.loop = true;
    snd.play();

    Gamedata.show();

    $('#gameinfo').show();

    var context = carte.getContext("2d");
    
    map.init(socket, context, player);
    player.init(socket, context);

    carte.addEventListener("click", map.playerClick, false);

    var tick = function() {
      context.clearRect(0, 0, carte.width, carte.height);
      
      map.trace(false);
      player.trace();

      window.requestNextAnimationFrame(tick);
    }

    window.requestNextAnimationFrame(tick);

    //Gestion des touches et des collisions
    window.onkeydown = function(event) {
      var e = event || window.event;
      var key = e.which || e.keyCode;
      
      switch(key) {
        case 37 : // GAUCHE
          console.log('left');
          player.move(0, 0);
          break;
        case 38 : // HAUT
          console.log('top');
          player.move(1, 0);
          break;
        case 39 : // DROITE
          console.log('right');
          player.move(1, 1);
          break;
        case 40 : // BAS
          console.log('bottom');
          player.move(0, 1);
          break;
        default : 
          console.log(key);
          return true;
      }
      
      return false;
    }

    var gamepad = new Gamepad();

    gamepad.bind(Gamepad.Event.CONNECTED, function(device) {
        console.log('device connected');
        console.log(device);

        $('#gameinfo .gamepad').show();
    });

    gamepad.bind(Gamepad.Event.DISCONNECTED, function(device) {
        console.log('device disconnected');
        console.log(device);

        $('#gameinfo .gamepad').hide();
    });

    gamepad.bind(Gamepad.Event.BUTTON_DOWN, function(e) {
        switch(e.mapping) {
          case 14 : // GAUCHE
            console.log('left');
            player.move(0, 0);
            break;
          case 12 : // HAUT
            console.log('top');
            player.move(1, 0);
            break;
          case 15 : // DROITE
            console.log('right');
            player.move(1, 1);
            break;
          case 13 : // BAS
            console.log('bottom');
            player.move(0, 1);
            break;
          default : 
            console.log(e);
            return true;
        }
    });

    if (!gamepad.init()) {
        console.log('Not Supported');
    }

    socket.on('naturalevent', function(data) {
      Gamedata.showMeteo(data.name);

      setTimeout(function() { Gamedata.showMeteo('Soleil'); }, 20000);
    });

    Gamedata.showMeteo('Soleil');
  }

  return this;
});