require(['js/utils/connexion.js'], function(connexion) {
  $('.login form .btn').bind('click', function(e) {
    e.preventDefault();

    switch($(this).attr('name')) {
      case 'connecter' :
        connexion.login();

        break;
      case 'register' :
        $('.login').hide();
        $('.register').show();

        break;
    }
  });

  $('.register form .btn').bind('click', function(e) {
    e.preventDefault();

    switch($(this).attr('name')) {
      case 'valider' :
        connexion.register();

        break;
      case 'cancel' :
        $('.register').hide();
        $('.login').show();

        break;
    }
  });

  $('.startgame form .btn').bind('click', function(e) {
    e.preventDefault();

    connexion.saveGamePref();
  });

  $('.game form .btn').bind('click', function(e) {
    e.preventDefault();

    connexion.initIo();
  });

  $('.startgame form .perso').bind('click', function(e) {
    e.preventDefault();

    $('.startgame form .perso').removeClass('selected');
    $(this).addClass('selected');

    var perso;

    switch ($(this).attr('title')) {
      case 'm' :
        perso = 1;
        break
      case 'f' :
        perso = 2;
        break
      default :
        perso = 1;
        break
    }

    $('.startgame form input[name=perso]').val(perso);
  });
});