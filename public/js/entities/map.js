define(['js/utils/gamedata.js'], function(GameData) {
  return function Map() {
    var self = this;

    var context;
    var socket;
    var mainPlayer;

    var mapData = Array();

    var hauteur = 26;
    var largeur = 11;

    var tw = 92;
    var th = 47;

    var grass = new Image();
    grass.src = 'img/grass_water_2.png';

    this.init = function(socket, context, mainPlayer) {
      this.context = context;
      this.mainPlayer = mainPlayer;
      this.socket = socket;

      this.socket.emit('getMap', null);

      this.socket.on('setTile', function(data) {
        if (mapData[data.posX] == undefined) {
          mapData[data.posX] = Array();
        }

        mapData[data.posX][data.posY] = data;
      });
    }

    this.trace = function(debug) {
      playerX = this.mainPlayer.playerX;
      playerY = this.mainPlayer.playerY;

      for (i = 0; i < hauteur; i++) {
        for (j = 0; j < largeur; j++) {
          var posX = Math.ceil(playerX-(largeur/2) + j);
          var posY = Math.ceil(playerY-(hauteur/2) + i);

          if (mapData[posX] != undefined) {
            if (mapData[posX][posY] != undefined) {
              if (i%2) {
                this.draw(mapData[posX][posY], j*tw-tw/2, i*th/2-th);
              } else {
                this.draw(mapData[posX][posY], j*tw-tw, i*th/2-th);
              }
            } else {
              if (debug) console.log('posY: '+posY);
            }
          } else {
            if (debug) console.log('posX: '+posX);
          }
        }
      }
    }

    this.draw = function(seed, xdepart, ydepart) {
      var tX = this.mainPlayer.tX;
      var tY = this.mainPlayer.tY;
      this.context.drawImage(grass, 276, 1310, 92, 47, xdepart-tX, ydepart-tY, 92, 47);
    }

    this.playerClick = function(e) {
      e.preventDefault();

      var x = e.pageX - this.offsetLeft;
      var y = e.pageY - this.offsetTop;

      var position = self.posInGrid(x, y, 86);
      
      GameData.showCoord(position.posX, position.posY);
      GameData.showFertility(mapData[position.posX][position.posY].fertility);
      GameData.showHumidity(mapData[position.posX][position.posY].humidity);
    }

    this.posInGrid = function(mousex, mousey) {
      // -1 signifie aucune position trouvé
      var posx = -1;
      var posy = -1;

      for (i = 0; i < hauteur; i++) {
        // position au dessus de -1 veux dire donné trouvé
        if (posx > -1 && posy > -1)
          break;

        for (j = 0; j < largeur; j++) {
          if (i%2) {
            if (j*tw-tw/2 <= mousex && mousex <= j*tw+tw/2 && i*th/2-th <= mousey && mousey <= i*th/2) {
              posx = i;
              posy = j;
              break;
            }
          } else {
            if (j*tw-tw <= mousex && mousex <= j*tw && i*th/2-th <= mousey && mousey <= i*th/2) {
              posx = i;
              posy = j;
              break;
            }
          }
        }
      }

      var tempx = (posx%2) ? posy*tw-tw/2 : posy*tw-tw;
      var tempy = posx*th/2-th

      // point central du rectangle
      var x1 = tempx + tw/2;
      var y1 = tempy + th/2;

      // point du clique
      var x2 = mousex;
      var y2 = mousey;

      if (x2 < x1) {
        if (this.segCut(x1, y1, x2, y2, tempx, tempy+th/2, tempx+tw/2, tempy+th)) {
          if (!(posx%2)) {
            --posy;
          }
          
          ++posx;
        }
      } else {
        if (this.segCut(x1, y1, x2, y2, tempx+tw/2, tempy+th, tempx+tw, tempy+th/2)) {
          if (posx%2) {
            ++posy;
          }
          
          ++posx;
        }
      }

      return {
              tileI: posy,
              tileJ: posx,
              posX: Math.ceil(playerX-(largeur/2)+posy),
              posY: Math.ceil(playerY-(hauteur/2)+posx)
          };
    }

    this.segCut = function(x1, y1, x2, y2, x3, y3, x4, y4) {
      a1 = (y2-y1)/(x2-x1);
      a2 = (y4-y3)/(x4-x3);

      if (a1 == a2) {
        return false;
      }

      b1 = y2-(a1*x2);
      b2 = y4-(a2*x4);

      xcommun = (b2-b1)/(a1-a2);

      if (x1 < x2) {
        if (x1<=xcommun && xcommun<=x2) {
          return true;
        }
      } else {
        if (x1>=xcommun && xcommun>=x2) {
          return true;
        }
      }

      return false;
    }
  }
});