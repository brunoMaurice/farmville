define(['js/utils/gamedata.js'], function(GameData) {
  return function Player() {
    var self = this;

    var persoImg = new Image();
    persoImg.src = 'img/perso.png';

    this.playerX = 0;
    this.playerY = 0;
    this.tX = 0;
    this.tY = 0;

    var persoSprite = [
      [0, 0],
      [16, 0],
      [32, 0],
      [48, 0],
      [64, 0],
      [80, 0],
      [0, 27],
      [16, 27],
      [32, 27],
      [48, 27],
      [64, 27],
      [80, 27],
    ];

    var animeTimer = 100;
    var move = false;

    var spriteN = 1;

    var context;
    var socket;

    var snd = new Audio('mp3/walk.mp3');

    this.init = function(socket, context) {
      this.context = context;

      this.socket = socket;

      this.socket.emit('getPos', null);

      this.socket.on('setPos', function(data) {
        self.playerX = data.posX;
        self.playerY = data.posY;

        GameData.showCoord(self.playerX, self.playerY);
      });
    };

    this.trace = function() {
      this.context.drawImage(persoImg, persoSprite[spriteN][0], persoSprite[spriteN][1], 16, 28, 452, 258, 16, 28);
    }

    this.move = function(px, py) {
      if (!move) {
        move = true;
        if (px == 1 && py == 0) {
          this.animation('top', 46, -23);
        } else if (px == 1 && py == 1) {
          this.animation('right', 46, 23);
        } else if (px == 0 && py == 1) {
          this.animation('bottom', -46, 23);
        } else if (px == 0 && py == 0) {
          this.animation('left', -46, -23);
        }

        snd.play();
      }
    }

    this.changeCoord = function(x, y) {
      this.playerX += x;
      this.playerY += y;

      this.tX = 0;
      this.tY = 0;

      move = false;
      snd.pause();
      snd.currentTime = 0;

      GameData.showCoord(this.playerX, this.playerY);

      this.socket.emit('setPos', {x: this.playerX, y: this.playerY});
      this.socket.emit('getMap', null);
    }

    this.animation = function(type, px, py) {
      switch (type) {
        case 'top' :
          if (spriteN == 3) {
            spriteN = 5;
          } else if(spriteN == 5) {
            spriteN = 10;
          } else {
            spriteN = 3;
          }

          if (px-7 > 0) {
            this.tX += 7;
            px -= 7;
          } else {
            this.tX += px;
            px = 0;
          }
          
          if (py+3.5 < 0) {
            this.tY -= 3.5;
            py += 3.5;
          } else {
            this.tY += py;
            py = 0;
          }

          if (px == 0 && py == 0) {
            spriteN = 3;

            if (this.playerY%2) {
              this.changeCoord(1, -1);
            } else {
              this.changeCoord(0, -1);
            }
            
            break;
          }
          
          setTimeout(function() {self.animation('top', px, py)}, animeTimer);

          break;
        case 'right' : 
          if (spriteN == 1) {
            spriteN = 8;
          } else if(spriteN == 8) {
            spriteN = 9;
          } else {
            spriteN = 1;
          }

          if (px-7 > 0) {
            this.tX += 7;
            px -= 7;
          } else {
            this.tX += px;
            px = 0;
          }
          
          if (py-3.5 > 0) {
            this.tY += 3.5;
            py -= 3.5;
          } else {
            this.tY += py;
            py = 0;
          }

          if (px == 0 && py == 0) {
            spriteN = 1;

            if (this.playerY%2) {
              this.changeCoord(1, 1);
            } else {
              this.changeCoord(0, 1);
            }
            
            break;
          }

          setTimeout(function() {self.animation('right', px, py)}, animeTimer);

          break;
        case 'bottom' : 
          if (spriteN == 0) {
            spriteN = 6;
          } else if(spriteN == 6) {
            spriteN = 7;
          } else {
            spriteN = 0;
          }

          if (px+7 < 0) {
            this.tX -= 7;
            px += 7;
          } else {
            this.tX += px;
            px = 0;
          }
          
          if (py-3.5 > 0) {
            this.tY += 3.5;
            py -= 3.5;
          } else {
            this.tY += py;
            py = 0;
          }

          if (px == 0 && py == 0) {
            spriteN = 0;

            if (this.playerY%2) {
              this.changeCoord(0, 1);
            } else {
              this.changeCoord(-1, 1);
            }

            break;
          }

          setTimeout(function() {self.animation('bottom', px, py)}, animeTimer);

          break;
        case 'left' : 
          if (spriteN == 2) {
            spriteN = 4;
          } else if(spriteN == 4) {
            spriteN = 11;
          } else {
            spriteN = 2;
          }

          if (px+7 < 0) {
            this.tX -= 7;
            px += 7;
          } else {
            this.tX += px;
            px = 0;
          }
          
          if (py+3.5 < 0) {
            this.tY -= 3.5;
            py += 3.5;
          } else {
            this.tY += py;
            py = 0;
          }

          if (px == 0 && py == 0) {
            spriteN = 2;

            if (this.playerY%2) {
              this.changeCoord(0, -1);
            } else {
              this.changeCoord(-1, -1);
            }

            break;
          }

          setTimeout(function() {self.animation('left', px, py)}, animeTimer);

          break;
        default :
          spriteN = 1;
          this.tX = 0;
          this.tY = 0;

          break;
      }
    }
  }
});