define(function() {
  return {
    gamedata : document.getElementById('gamedata'),

    show : function() {
      this.gamedata.style.display = 'block';
    },
    hide : function() {
      this.gamedata.style.display = 'none';
    },
    showCoord : function(x, y) {
      this.gamedata.querySelector('.coordX').innerHTML = x;
      this.gamedata.querySelector('.coordY').innerHTML = y;
    },
    showFertility : function(value) {
      this.gamedata.querySelector('.fertility').innerHTML = value;
    },
    showHumidity : function(value) {
      this.gamedata.querySelector('.humidity').innerHTML = value;
    },
    showMeteo : function(value) {
      this.gamedata.querySelector('.meteo').innerHTML = value;
    },
    addDebug : function(element) {
      var debug = this.gamedata.querySelector('.debug');

      if (debug == null) {
        this.gamedata.innerHTML += '<br />Debug : <span class=\'debug\'></span>';
        debug = this.gamedata.querySelector('.debug');
      }

      debug.innerHTML += '<br />'+element;
    }
  }
});