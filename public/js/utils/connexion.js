define (['game'], function (game) {

  var connexion = function() {
    var socket = null;
    var socketUrl = "http://localhost:3000/";

    this.initIo = function() {
      socket = io.connect(socketUrl);
      game.run(socket);
    };

    this.getIo = function() {
      return socket;
    }

    this.register = function() {
      var formSelector = '.register form';
      var email = $(formSelector+' input[name=email]').val();
      var pwd = $(formSelector+' input[name=pwd]').val();
      var pwd2 = $(formSelector+' input[name=pwd2]').val();
      var user = $(formSelector+' input[name=user]').val();

      var data = 'email='+email+'&pwd='+pwd+'&pwd2='+pwd2+'&user='+user;

      if (pwd == pwd2) {
        $.ajax({
          type: 'POST',
          url: 'http://'+location.host+'/register',
          data: data,
          success: function(msg){
            if (msg.statue == 'ok') {
              $(formSelector+' .title-msg').html('Votre compte est enregistré');

              setTimeout(function(){
                $('.login').show();
                $('.register').hide();

                $(formSelector+' input[name=email]').val('');
                $(formSelector+' input[name=pwd]').val('');
                $(formSelector+' input[name=pwd2]').val('');
                $(formSelector+' input[name=user]').val('');
                
                $(formSelector+' .title-msg').html('');
              }, 2000);
            }
          }, error: function(msg) {
            $(formSelector+' .title-msg').html('Erreur d\'enregistrement');
          }
        });
      } else {
        $(formSelector+' .title-msg').html('Formulaire invalide');
      }
    };

    this.login = function() {
      var formSelector = '.login form';
      var email = $(formSelector+' input[name=email]').val();
      var pwd = $(formSelector+' input[name=pwd]').val();

      var data = 'email='+email+'&pwd='+pwd;
      var co = this;

      $.ajax({
        type: 'POST',
        url: 'http://'+location.host+'/login',
        data: data,
        success: function(msg){
          if (msg.statue == 'ok') {
            $(formSelector+' .title-msg').html('Connexion validé');

            setTimeout(function(){
              $('.login').hide();

              $(formSelector+' input[name=email]').val('');
              $(formSelector+' input[name=pwd]').val('');
              
              $(formSelector+'.title-msg').html('');

              $('.nav').append(msg.menu);

              if (msg.game) {
                co.initIo();
              } else {
                $('.startgame').show();
              } 
            }, 500);
          } else {
            $(formSelector+' .title-msg').html('Connexion Invalide');
          }
        }, error: function(msg) {
          $(formSelector+' .title-msg').html('Erreur de connexion');
        }
      });
    };

    this.saveGamePref = function() {
      var formSelector = '.startgame form';
      var perso = $(formSelector+' input[name=perso]').val();
      var difficulty = $(formSelector+' input[name=difficulty]:checked').val();

      var data = 'perso='+perso+'&difficulty='+difficulty;
      var co = this;

      $.ajax({
        type: 'POST',
        url: 'http://'+location.host+'/savegamepref',
        data: data,
        success: function(msg){
          if (msg.statue == 'ok') {
            $(formSelector+' .title-msg').html('Préférence sauvegardé');

            setTimeout(function(){
              $('.startgame').hide();
              
              co.initIo();
            }, 1000);
          }
        }, error: function(msg) {
          $(formSelector+' .title-msg').html('Erreur');
        }
      });
    }
  };

  return new connexion();
});