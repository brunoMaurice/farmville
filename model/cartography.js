var mongoose = require(".././config/db.js").mongoose;

var CartographySchema = mongoose.Schema({
      posX        : Number,
      posY        : Number,
      humidity    : Number,
      fertility   : Number,
      health	  : Number,
      _cropid     : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Crop',
        default : null
      },
      _userid     : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        default : null
      },
      _baseid     : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Base'
      }
})

CartographySchema.methods.updateHealth = function() {
  this.health = round((this.fertility + this.humidity) * 0.5);

  this.save();
  return this;
}

CartographySchema.methods.fertilize = function() {
  this.fertility += 10;

  if(this.fertility > 100) {
    this.fertility = 100;
  }
  
  //this.save(); //probably don't need to save as we'll save during the updateHealth method
  this.updateHealth();
  return this;
}

CartographySchema.methods.waterize = function() {
  this.humidity += 10;

  if(this.humidity > 100) {
    this.humidity = 100;
  }

  //this.save();
  this.updateHealth();
  return this;
}

var CartographyModel = mongoose.model('Cartography', CartographySchema);

exports.Cartography = CartographyModel;