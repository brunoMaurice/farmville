var mongoose = require(".././config/db.js").mongoose;

var WeaponSchema = mongoose.Schema({
      power       : Number,
      hitRatio    : Number,
      hitsPerSec  : Number,
      price       : Number
})

var WeaponModel = mongoose.model('Weapon', WeaponSchema);

exports.Weapon = WeaponModel;