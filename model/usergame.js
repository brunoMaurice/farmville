var mongoose = require(".././config/db.js").mongoose;

var UsergameSchema = mongoose.Schema({
      _userid     : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }],
      perso       : Number,
      level       : Number,
      money       : Number,
      posX        : Number,
      posY        : Number,
	  _weaponid	  : [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Weapon'
      }],
	  health	  : { type: Number, default: 5}
})

UsergameSchema.methods.levelUp = function() {
  this.level+=1;
  this.health = this.level*5;

  this.save();
  return this;
}

var UsergameModel = mongoose.model('Usergame', UsergameSchema);

exports.Usergame = UsergameModel;