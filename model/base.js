var mongoose = require(".././config/db.js").mongoose;

var BaseSchema = mongoose.Schema({
      num    : Number,
      centerX : Number,
      centerY : Number,
      used    : {
        type  : Boolean,
        default : 0
      }
})

var BaseModel = mongoose.model('Base', BaseSchema);

exports.Base = BaseModel;