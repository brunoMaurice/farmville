var mongoose = require(".././config/db.js").mongoose;

var UserSchema = mongoose.Schema({
			username   	: String,
	    email       : String,
	    password 		: String,
	    type      	: {
	    	type : Boolean,
	    	default : 0
	    },
	    date		: {
	    	type : Date
	    },
	    _usergame		: {
	    	type: mongoose.Schema.Types.ObjectId,
	    	ref: 'Usergame'
	    }
})

var UserModel = mongoose.model('User', UserSchema);

exports.User = UserModel;