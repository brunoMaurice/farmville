var mongoose = require(".././config/db.js").mongoose;

var CropSchema = mongoose.Schema({
      growRate       : Number,
      decayTime      : Number,
      productivity   : Number,
      storability    : Number,
      seedPrice      : Number,
      name	         : String
})

var CropModel = mongoose.model('Crop', CropSchema);

exports.Crop = CropModel;