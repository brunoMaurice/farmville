var mongoose = require(".././config/db.js").mongoose;

var OptionsSchema = mongoose.Schema({
      name        : String,
      value       : String,
})

var OptionsModel = mongoose.model('Options', OptionsSchema);

exports.Options = OptionsModel;