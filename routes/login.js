var crypto = require('crypto');

exports.register = function(req, res){
  var User = require('.././model/user.js').User;

  var shasum = crypto.createHash('sha1');
  shasum.update(req.body.pwd);

  var user = new User({
    username    : req.body.user,
    email       : req.body.email,
    password    : shasum.digest('hex'),
    type        : 1,
    date        : new Date(),
    _usergame   : null
  });

  user.save(function(error, user) {
    if (error) {
      console.log(error);
      res.json(500, { error: 'Invalid Post' })
    } else {
      console.log('User '+user.username+' added !');
      res.json(200, { statue: 'ok' });
    }
  });
}

exports.login = function(req, res){
  var User = require('.././model/user.js').User;

  var shasum = crypto.createHash('sha1');
  shasum.update(req.body.pwd);

  var email = req.body.email;
  var password = shasum.digest('hex');

  User.where('email', email)
    .where('password', password)
    .findOne().populate('_creator')
    .exec(function(error, user) {
      if (error) {
        console.log(error);
        res.json(500, { error: 'Error Connexion' })
      } if (user) {
        console.log(user);
        console.log('User '+user.username+' logged !');
        req.session.user = user;

        var menu = '<li><a href="/logout">Deconnexion</a></li>';

        if (user.type == 1)
          menu += '<li><a href="/admin">Panel Admin</a></li>'

        var havegame = false;

        if (user._usergame != null) {
          havegame = true;

          req.session.user = user;
        }

        res.json(200, { statue: 'ok', menu: menu, game: havegame });   
      } else {
        res.json(200, { statue: 'User Invalid' });
      }
    });
}

exports.logout = function(req, res){
  if (req.session.user)
    delete req.session.user;
  
  res.redirect('/');
}

exports.savegamepref = function(req, res){
  if (req.session.user._usergame != null)
    res.json(200, { statue: 'Can\'t create another game' });

  var Usergame = require('.././model/usergame.js').Usergame;
  var User = require('.././model/user.js').User;
  var Options = require('.././model/options.js').Options;
  var Base = require('.././model/base.js').Base;

  Options.findOne({name: 'startMoney'}, function (error, option) {
    if (error) {
      console.log(error);
      res.json(500, { error: 'Invalid Post' });
    } else {
      Base.find({used:false}).sort({num:1}).limit(1).exec(function(error, base) {
        if (error) {
          console.log(error);
          res.json(500, { error: 'Invalid Post' });
        } else {
          var startMoney = option.value;

          var gameDifficulty = Array();
          gameDifficulty['easy'] = 100;
          gameDifficulty['medium'] = 50;
          gameDifficulty['hard'] = 10;

          var usergame = new Usergame({
            _userid     : req.session.user._id,
            perso       : req.body.perso,
            money       : startMoney*gameDifficulty[req.body.difficulty]/100,
            level       : 1,
            posX        : base[0].centerX,
            posY        : base[0].centerY
          });

          usergame.save(function(error, usergame) {
            if (error) {
              console.log(error);
              res.json(500, { error: 'Invalid Post' });
            } else {
              User.findOne({_id: req.session.user._id}, function (error, user) {
                if (!error) {
                  base[0].used = true;
                  base[0].save();

                  user._usergame = usergame._id;
                  user.save(function(error, user) {
                    req.session.user = user;
                    res.json(200, { statue: 'ok' });
                  });
                } else {
                  res.json(500, { error: 'Invalid Post' });
                }
              });
            }
          });
        }
      });
    }
  });
}