/*
 * GET home page.
 */

exports.index = function(req, res){
  var user = (req.session.user) ? req.session.user : 'null';

  res.render('index', { title: 'Farm Ville', user: user });
};