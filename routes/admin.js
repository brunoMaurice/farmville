exports.index = function(req, res){
  if (req.session.user.type != 1)
    res.json(200, { statue: 'Wrong access' });

  var Options = require('.././model/options.js').Options;

  Options.findOne({name: 'startMoney'}, function (error, option) {
    if (!error) {
      res.render('admin/index', { title: 'Farm Ville - Admin', startmoney: option.value });
    } else {
      res.json(404, { statue: 'Not Found' });
    }
  });
};

exports.save = function(req, res){
  if (req.session.user.type != 1)
    res.json(200, { statue: 'Wrong access' });

  var Options = require('.././model/options.js').Options;

  Options.update({ name : 'startMoney' }, { value : req.body.startmoney });

  res.render('admin/index', { title: 'Farm Ville - Admin', startmoney: req.body.startmoney });
};