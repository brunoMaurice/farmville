var map = require('.././utils/map.js');
var Options = require('.././model/options.js').Options;

module.exports = function() {
  Options.findOne({name: 'init'}, function (error, option) {
    if (option == null) {
      map.getMap(0, 0);

      var options = new Options({
        name     : 'init',
        value    : 1
      });

      options.save();

      var options = new Options({
        name     : 'startMoney',
        value    : 100
      });

      options.save();
    }
  });
};