var routes = require('.././routes');
var loginroutes = require('.././routes/login');
var adminroutes = require('.././routes/admin');

module.exports = function(app) {
	app.get('/', routes.index);
  app.post('/register', loginroutes.register);
  app.post('/login', loginroutes.login);

  // User Accèes
  app.get('/logout', loginroutes.logout);
  app.post('/savegamepref', loginroutes.savegamepref);

  // Admin Module
  app.get('/admin', adminroutes.index);
  app.post('/admin', adminroutes.save);
};