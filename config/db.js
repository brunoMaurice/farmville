var mongoose = require("mongoose");

var host = 'localhost';
var database = 'farmville';

mongoose.connect(host, database);

exports.mongoose = mongoose;