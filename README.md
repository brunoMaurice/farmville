Farm Ville Project
====================

1 Project Overview
---------------------

Cummorah Interactive is an innovative company founded to create and manage multi-player games.
Their last project is web-based multiplayer strategy/management game that involves farmers.

Your Team has been selected as a subcontractor that will endorse the development based on the functional expression

The Game is about creating and managing a farm (growing, harvesting, ...) and trying to extend it as much as possible. The game will involve territory management (conquest and defense) as well as alliances.

The game has to be developed with the following technologies:

  - Server side: A JS Webserver, node.js or Wakanda
  - Client side: HTML5 (Canvas API)/JS

You're free to use any existing library built on top of the above mentioned technologies.

2 Functional Expression
---------------------

### 2.1 User Accounts

User need to register to play the game. They register using their mail address and a password. There are two user type :

  - Users
  - Administrators

Users can create only one game per account. They start a ne game (or restart when they've destroyed the current game) by selecting a level :

  - Easy start with 100% of start money
  - Medium start with 50% of start money
  - Hard start with 10% of start money

Administrators have the power to modify the value of everything in the game: start money, weapons, cost and properties, ...

More Will Come Soon =D